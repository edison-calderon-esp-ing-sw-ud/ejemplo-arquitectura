# Tienda de Barrio
Proyecto de tienda de barrio visto desde perspectiva Archimate y una pequeña implementación.

Presentado por: Edison Smith Calderón Sánchez

# Vistas Archimate
A continuación se ofrecen varias vistas desde los puntos de vista Archimate.

## Organización
### Tienda
![Diagrama de organización de la Tienda](/diagramas/organizacion_tienda.png)

### Domicilio
![Diagrama de organización del Domicilio](/diagramas/organizacion_domicilio.png)

## Cooperación entre actores - Despacho de la mercancía
![Diagrama de cooperación entre actores - Despacho de la mercancía](/diagramas/cooperacion_actores_despacho.png)

## Funciones de negocio
![Diagrama de funciones de negocio](/diagramas/funciones_negocio.png)

## Procesos de negocio
![Diagrama de procesos de negocio](/diagramas/procesos_negocio.png)

## Cooperación procesos de negocio
![Diagrama de cooperación procesos de negocio](/diagramas/cooperacion_procesos_negocio.png)

## Producto
![Diagrama de producto](/diagramas/producto.png)

## Comportamiento de Aplicación
![Diagrama de comportamiento de aplicación](/diagramas/comportamiento_aplicacion.png)

## Cooperación de Aplicaciones
![Diagrama de cooperación de aplicaciones](/diagramas/cooperacion_aplicaciones.png)

## Estructura de Aplicación
![Diagrama de estructura de aplicación](/diagramas/estructura_aplicacion.png)

## Uso de Aplicación
![Diagrama de uso de aplicación](/diagramas/uso_aplicacion.png)

## Infraestructura
![Diagrama de infraestructura](/diagramas/infraestructura.png)

## Uso de infraestructura
![Diagrama de uso de infraestructura](/diagramas/uso_infraestructura.png)

## Organización e Implementación
![Diagrama de organización e Infraestructura](/diagramas/organizacion_implementacion.png)

## Estructura de Información
![Diagrama de estructura de información](/diagramas/estructura_informacion.png)

## Realización de servicios
![Diagrama de realización de servicios](/diagramas/realizacion_servicios.png)

## Por capas
![Diagrama por capas](/diagramas/por_capas.png)